/*
This server will recursively check if there are any new listings and post them in this discord if they are listed below a floor value.
Setup DISCORD_BOT_LOGIN_KEY and LISTING_CHANNEL_ID in .env file, additional parameters in params section
To run the server we simply call "node ExampleServer.js"
*/

import ExampleBot from "./ExampleBot.js"
import ImxCore from "./ImxCore.js";
import {createRequire} from "module"; // Bring in the ability to create the 'require' method

const require = createRequire(import.meta.url); // construct the require method

require("dotenv").config();
let core = new ImxCore()

let exampleBot = null

if (process.env.DISCORD_BOT_LOGIN_KEY) {
  exampleBot = new ExampleBot(core)
} else {
  console.log("no DISCORD_BOT_LOGIN_KEY")
}
if (!process.env.LISTING_CHANNEL_ID) {
  console.log("no LISTING_CHANNEL_ID")
}

let listCollectionsChannels = {}
listCollectionsChannels[core.gogsHeroesCollectionAddress] = process.env.LISTING_CHANNEL_ID

// params section
const dataRefreshDelaySeconds = 60 // how often we refresh floorData
const fetchOrdersDelaySeconds = 1 // how often we check for new orders
const belowFloorFactor = 0.99 // when do we show new listed asset? 0.99 would mean we list the asset if the price is 99% or less of the current floor value. 

await core.getImxTokens()
dataRefreshRecursive()
await core.sleep(1000)
fetchOrdersRecursive()

async function dataRefreshRecursive() {
  console.log(`Refreshing data... ${new Date().toUTCString()}`)
  await core.fetchCryptoPrices()
  await core.fetchFloors(core.gogsHeroesCollectionAddress)
  await core.sleep(1000 * dataRefreshDelaySeconds)
  dataRefreshRecursive()
}

async function fetchOrdersRecursive() {
  try {
    let orders = await core.fetchOrders("https://api.x.immutable.com", null, core.gogsHeroesCollectionAddress)

    if(orders && orders.length !== 0) {
      console.log("new orders!");
      core.updateLastAssetUpdateTime(orders[0].updated_timestamp)
      let formattedAssets = await Promise.all(orders.map((order) => core.formatAsset(order, true)))

      formattedAssets = formattedAssets.filter(asset => asset != null)
      formattedAssets = formattedAssets.filter(asset => core.useTokens.includes(asset.buy_token_symbol));

      for (let asset of formattedAssets) {
        if(asset.order.status === "active") {
          if(listCollectionsChannels[asset.token_address]) {
            let currentPriceUSD = core.convertNormalTokenValueToUsdValue(asset.floorValue.ETH, "ETH")
            let currentPriceThisTokenUSD = core.convertNormalTokenValueToUsdValue(asset.floorValue[asset.buy_token_symbol], asset.buy_token_symbol)

            if(currentPriceUSD > 1 && asset.priceUSD<belowFloorFactor*currentPriceUSD && asset.priceUSD<belowFloorFactor*currentPriceThisTokenUSD) {
              exampleBot.listNewOrder(listCollectionsChannels[asset.token_address], asset, true, "active")
            } else {
              console.log("asset is not below floor")
            }
          }
        } else if(asset.order.status === "filled") {
          //filled
        } else if(asset.order.status === "cancelled") {
          //cancelled
        } 
      }
    } else {
      console.log("No new orders!");
    }
  } catch(err) {
    if (err.toString().includes("Unexpected end of JSON")) {
      console.log(`fetchOrdersRecursive, We probably hit rate limit ${new Date().toUTCString()}`)
      await core.sleep(1000*60*5) // we timeout for 5 min
    } else if (err.toString().includes("Error: AbortError:")) {} else console.log("fetchOrdersRecursive error: ", err);
  }

  await core.sleep(1000*fetchOrdersDelaySeconds)
  fetchOrdersRecursive()
}